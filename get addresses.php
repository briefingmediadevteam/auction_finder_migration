<?php

/**
 * Created by PhpStorm.
 * User: sthackeray
 * Date: 21/12/2015
 * Time: 14:49
 */

class uploadMarts
{

    public $mysqli;
    public $old_db_mysqli;
    public $address;
    public $details;
    public $file;
    public $dateStamp;

    function __construct()
    {
        $this->file = "temp/data/marts.csv";
        $this->address = array();
        $this->details = array();
        $this->mysqli = new mysqli('localhost', 'stephen', 'password', 'auction_finder');
        $this->old_db_mysqli = new mysqli('localhost', 'stephen', 'password', 'OLD_auction_finder');
        $this->dateStamp = date("Y-m-d h:m:s", time());
    }


    function loadFile($file)
    {
        if (($handle = fopen($file, "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                $marts[] = $data;
            }
        }
        return $marts;
    }

    function getGeocodedData($address)
    {

     // For marts/companies
     /*   if ($row[4] == 'N/A' || $row[4] == "") {
            $address = $row[5] . ", " . $row[7];
        } else {
            $address = $row[4] . ", " . $row[5] . ", " . $row[7];
        }
        $address = str_replace(", ,", ",", $address);
     */

        // For breed societies
        // $address = $row[2].", ".$row[3];


        $url = "https://maps.googleapis.com/maps/api/geocode/json?";
        $url .= "address=" . urlencode($address) . "&key=AIzaSyB1UFVIkzllWnOPIXNW2NSMIkeSHo-ZeuI";
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $url
        ));
        $result = curl_exec($curl);
        curl_close($curl);
        $results = json_decode($result);
        return $results;
    }

    function checkIfExists($table, $col, $val)
    {
        $mysqli = $this->mysqli;
        $sql = "SELECT id," . $col . " FROM " . $table . " WHERE " . $col . "='" . $val . "'";
        if ($result = $mysqli->query($sql)) {
            //   echo $result->num_rows;
            if ($result->num_rows == 0) {
                $data = 0;
            } else {
                while ($row = $result->fetch_assoc()) {
                    $data = $row['id'];
                }
            }
            $result->close();
        } else die($mysqli->error);
        return $data;

    }

    function setUser($user)
    {
        $user = strtolower($user);
        $userExists = $this->checkIfExists('users', 'username', $user);
        $password = md5("password");

        if ($userExists == 0) {
            $mysqli = $this->mysqli;
            $sql = "INSERT INTO users VALUES(
          '',
          '" . $user . "',
          '" . $password . "',
          1,
          '" . $this->dateStamp . "',
          ''
  )";
            if ($result = $mysqli->query($sql)) {
                echo "user " . $user . " added to users table<br>";
                return $mysqli->insert_id;
            } else {
                die($mysqli->error);
            }
        } else {
            echo "User " . $user . " already exists in user table<br>";
            return $userExists;
        }

    }

    function setCountry($country)
    {
        $country = strtolower($country);
        $mysqli = $this->mysqli;
        $countryExists = $this->checkIfExists('countries', 'country', $country);
        if ($countryExists == 0) {
            $sql = "INSERT INTO countries VALUES('', '" . $country . "')";

            if ($result = $mysqli->query($sql)) {
                echo "Country " . $country . " added to country table<br>";
                return $mysqli->insert_id;
            } else {
                die($mysqli->error);
            }
        } else {
            echo "Country " . $country . " already exists in country table<br>";
            return $countryExists;
        }

    }

    function getCountry($countryID)
    {
        $country = '';
        $mysqli = $this->mysqli;
        $sql = "SELECT country FROM countries WHERE id=" . $countryID;
        if ($result = $mysqli->query($sql)) {
            while ($row = $result->fetch_assoc()) {
                $country = $row['country'];

            }
        }
        return $country;

    }

    function setRegion($region, $geoData, $countryID)
    {

        $region = strtolower($region);
        $mysqli = $this->mysqli;
        $regionExists = $this->checkIfExists('regions', 'region', $region);
        if ($regionExists == 0) {
            $sql = "INSERT INTO regions VALUES('', '" . $region . "', " . $countryID . ")";

            if ($result = $mysqli->query($sql)) {
                echo "Region " . $region . " added to region table<br>";
                return $mysqli->insert_id;
            } else {
                die($mysqli->error);
            }
        } else {
            echo "Region " . $region . " already exists in region table<br>";
            return $regionExists;
        }

    }

    function setCounty($county, $geoData, $regionID, $countryID)
    {
        $alt_county = '';
        $geoAddress = '';
        $addressParts = '';
        $county = strtolower($county);
     /*   if ($geoData[0]->status == 'OK') {
            $geoAddress = $geoData[0]->results[0]->address_components;
            foreach ($geoAddress as $addressParts) {
                if ($addressParts->types[0] == 'administrative_area_level_2') {
                    $county = $addressParts->long_name;
                }
            }
        } else {
            echo "GEOCODED DATA NOT AVAILABLE";
        }
*/
        $country = $this->getCountry($countryID);

        // Google Geocodes Scotland municipal areas rather than counties as areas, so ignore county check for Scotland
        /*  if ($country != 'scotland') {
              if ($county != $alt_county && $alt_county != '') {
                  $county = $alt_county;
              }
          }
  */
        $mysqli = $this->mysqli;

        $countyExists = $this->checkIfExists('counties', 'county', $county);
        if ($countyExists == 0) {
            $sql = "INSERT INTO counties VALUES('', '" . $county . "', '" . $regionID . "')";

            if ($result = $mysqli->query($sql)) {
                echo "County " . $county . " added to county table<br>";
                return $mysqli->insert_id;
            } else {
                die($mysqli->error);
            }
        } else {
            echo "County " . $county . " already exists in county table<br>";
            return $countyExists;
        }

    }


    function setCity($city, $geoData, $countyID)
    {
        $city = strtolower($city);
   /*     if ($geoData[0]->status == 'OK') {
            $geoAddress = $geoData[0]->results[0]->address_components;
            foreach ($geoAddress as $addressParts) {
                if ($addressParts->types[0] == 'locality') {
                    $city = strtolower($addressParts->long_name);
                    $city = addslashes($city);
                }
            }
        }
*/

        $mysqli = $this->mysqli;
        $cityExists = $this->checkIfExists('cities', 'city', $city);
        if ($cityExists == 0) {
            $sql = "INSERT INTO cities VALUES('', '" . $city . "', " . $countyID . ")";
            if ($result = $mysqli->query($sql)) {
                echo "City " . $city . " added to cities table<br>";
                return $mysqli->insert_id;
            } else {
                die($mysqli->error);
            }
        } else {
            echo "City " . $city . " already exists in cities table<br>";
            return $cityExists;
        }

    }

    function setAddress($address, $geoData, $locationIDs)
    {
        $mysqli = $this->mysqli;
        $address = strtolower($address);
        $street_number = '';
        $premise = '';
        $street = '';
        $postcode = '';
        $postcodeExists = $this->checkIfExists('address', 'postcode', $locationIDs['postcode']);

   //     if ($postcodeExists == 0 && $locationIDs['postcode'] != '') {
print_r($geoData);
            // Checks if there is geocoded data to override entered data
           if ($geoData->status == 'OK') {
                $geoAddress = $geoData->results[0]->address_components;
               foreach ($geoAddress as $addressParts) {
                    if ($addressParts->types[0] == 'street_number') {
                        $street_number = $addressParts->long_name;
                    }
                    if ($addressParts->types[0] == 'route') {
                        $street = $addressParts->long_name;
                    }
                    if ($addressParts->types[0] == 'premise') {
                        $premise = $addressParts->long_name;
                    }
                    if ($addressParts->types[0] == 'postcode') {
                        $postcode = $addressParts->long_name;
                    }

                }
                $longitude = ($geoData->results[0]->geometry->location->lng);
                $latitude = ($geoData->results[0]->geometry->location->lat);

                // If geocode fails enter address details in street field
                if ($street_number == '' && $street == '' && $premise == '') {
                    $street = $address;
                    echo "<font color='#ff0000'>Geocoding of address failed</font>";
                }

                $street = addslashes($street);

                if ($postcode == '') {
                    $postcode = $locationIDs['postcode'];
                }

                $sql = "INSERT INTO address VALUES('',
                  '" . $street_number . "',
                  '" . $premise . "',
                  '" . $street . "',
                  " . $locationIDs['cityID'] . ",
                  " . $locationIDs['countyID'] . ",
                  " . $locationIDs['regionID'] . ",
                  " . $locationIDs['countryID'] . ",
                  '" . $postcode . "',
                  " . $longitude . ",
                  " . $latitude . ",
                  '" . $this->dateStamp . "',
                  ''
            )";
echo $sql;

                if ($result = $mysqli->query($sql)) {
                    echo "Address " . $address . " added to cities table<br>";
                    return $mysqli->insert_id;
                } else {
                    die($mysqli->error);
                }
            } else {
                echo "Address " . $address . " already exists in cities table<br>";
                return $postcodeExists;
            }
  /*      } else {
            return $postcodeExists;
        }
*/
    }

    function setCompany($company, $user, $userID, $addressID, $data)
    {
        $company = addslashes(strtolower($company));
        $mysqli = $this->old_db_mysqli;
        $new_mysqli = $this->mysqli;
        $oldCoData = array(
            'email' => '',
            'c_description' => '',
            'picture',
            'laa'
        );

        $sql = "SELECT
              email,
              users.c_description,
              users.picture,
              users.laa,
              users.accessLevel
              FROM locations
              JOIN users ON user_id = users.id
              WHERE email = '".$user."'
              LIMIT 1
        ";

        if ($result = $mysqli->query($sql)) {
            while ($row = $result->fetch_assoc()) {
                if ($row['picture']!=null) {$oldCoData['picture'] = $row['picture'];}
                $oldCoData['c_description'] = $row['c_description'];
                if ($row['laa']!=null) {$oldCoData['laa'] = $row['laa'];}
            }
           } else {
            die($mysqli->error);
        }

        $companyID = $this->checkIfExists('company', 'company_name', $company);

        if ($companyID == 0) {
        /*    $sql = "INSERT INTO company VALUES(
              '',
              ".$userID.",
              '".addslashes($company)."',
              '".addslashes(str_replace('logos/', '', $oldCoData['picture']))."',
              '".addslashes($oldCoData['c_description'])."',
              '".$oldCoData['laa']."',
              '".$this->dateStamp."'
            )"; */

            $sql = "INSERT INTO company VALUES(
              '',
              ".$userID.",
              '".addslashes($company)."',
              '".$data[4]."',
              '".$data[6]."',
              '',
              '',
              0,
              '".$this->dateStamp."'
            )";

            if ($result = $new_mysqli->query($sql)) {
                echo "Company " . $company . " added to companies table<br>";
                $companyID =  $new_mysqli->insert_id;
            } else {
                die($new_mysqli->error);
            }

            // Add company/address to pivot table
            $sql = "INSERT INTO address_to_company VALUES('',
                      ".$companyID.",
                      ".$addressID.");
                      ";

           if ($result = $new_mysqli->query($sql)) {
                echo "Company " . $company . " added to company/address table<br>";
            } else {
                die($new_mysqli->error);
            }

        } else {
            echo "Company ".$company." already exists<br>";
        }

        return $companyID;
    }

    function setMart($data, $companyID, $addressID)
    {
        $mysqli = $this->mysqli;
        $company = addslashes(strtolower($data[0]));

        if ($data[0] == '') {
            $martExists = $this->checkIfExists('venues', 'email', $data[5]);
         //   $martExists = 0;
        } else {
            $martExists = $this->checkIfExists('venues', 'venue_name', $company);
        }

        if ($martExists == 0) {


            $sql = "INSERT INTO venues VALUES(
              '',
              1,
              '" . addslashes($company) . "',
              '" . addslashes($data[5]) . "',
              '" . addslashes($data[4]) . "',
              '" . addslashes($data[6]) . "',
              '',
              '" . $this->dateStamp . "',
              '')
            ";

            if ($result = $mysqli->query($sql)) {
                echo "Mart " . $company. " added to marts table<br>";
                $martID = $mysqli->insert_id;
            } else {
                die($mysqli->error);
            }

            // Add ids to mart to address table
                $sql = "INSERT INTO address_to_venue VALUES(
                  '',
                   ".$addressID.",
                   ".$martID."
                )";

            if ($result = $mysqli->query($sql)) {
                echo "Mart " . $company. " added to marts/address table<br>";
            } else {
                die($mysqli->error);
            }

            // Add ids to company to mart table
            $sql = "INSERT INTO company_to_venue VALUES(
                  '',
                   ".$companyID.",
                   ".$martID."
                )";

            if ($result = $mysqli->query($sql)) {
                echo "Mart " . $company. " added to company/marts table<br>";
            } else {
                die($mysqli->error);
            }

        } else {
            $martID = $martExists;
            echo "Mart ".$data[3]." already exists<br>";
        }
    }

    function setBreedingSociety($rowData, $userID, $type, $addressID) {
        $mysqli = $this->mysqli;
        $breedsoc = addslashes(strtolower($rowData[0]));
        $breedSocExists = $this->checkIfExists('breed_societies', 'society', $breedsoc);

        if ($breedSocExists == 0) {

            $sql = "INSERT INTO breed_societies VALUES(
                '',
                $userID,
                $type,
                '" . $breedsoc . "',
                '" . $rowData[4] . "',
                '',
                '" . $rowData[1] . "',
                '',
                '".$this->dateStamp."',
                '')";

            if ($result = $mysqli->query($sql)) {
                echo "Breed society " . $breedsoc. " added to breed_society table<br>";
                $breedSocID = $mysqli->insert_id;
            } else {
                die($mysqli->error);
            }

            $sql = "INSERT INTO address_to_breed_society VALUES(
                    '',
                     $breedSocID,
                     $addressID
                    );";

            if ($result = $mysqli->query($sql)) {
                echo "Breed society " . $breedsoc. " added to breed_society/address table<br>";
            } else {
                die($mysqli->error);
            }


        } else {
            $breedSocID = $breedSocExists;
        }
        return $breedSocID;
    }

}


// Script for loading marts
$martData = new uploadMarts();
$data = $martData->loadFile("temp/data/marts.csv");
$count = 0;

foreach ($data as $row) {
    $results = [];

    if ($count > -1) {
        $results[] = $martData->getGeocodedData($row);
        $userID = $martData->setUser($row[8]);
        $countryID = $martData->setCountry($row[0]);
        $regionID = $martData->setRegion($row[1], $results, $countryID);
        $countyID = $martData->setCounty($row[6], $results, $regionID, $countryID);
        $cityID = $martData->setCity($row[5], $results, $countyID);
        $addressID = $martData->setAddress($row[4], $results, array(
            'cityID' => $cityID,
            'countryID' => $countryID,
            'regionID' => $regionID,
            'countyID' => $countyID,
            'postcode' => $row[7]));
        $companyID = $martData->setCompany($row[2], $row[8], $userID, $addressID);
        $martID = $martData->setMart($row, $companyID, $addressID);
    }
    $count++;
    ob_flush();
    flush();
}


// Script for loading cattle breed societies
/* $breedsoc = new uploadMarts();
$data = $breedsoc->loadFile("temp/data/cattle_societies.csv");
$count = 0;

foreach ($data as $row) {
    $address = $row[2];

    $postcode = '';

    if ($count > -1) {
        $results = [];
    $results[] = $breedsoc->getGeocodedData($row);

        $findPostcode = $results[0]->results[0]->address_components;
        foreach ($findPostcode as $addressItem) {
            if ($addressItem->types[0] == "postal_code") {
                $postcode = $addressItem->long_name;
            }
        }


        $userID = $breedsoc->setUser($row[1]);
        $countryID = $breedsoc->setCountry('United Kingdom');
        $regionID = $breedsoc->setRegion('United Kingdom', $results, $countryID);
        $countyID = $breedsoc->setCounty('', $results, $regionID, $countryID);
        $cityID = $breedsoc->setCity('', $results, $countyID);
        $addressID = $breedsoc->setAddress($address, $results, array(
            'cityID' => $cityID,
            'countryID' => $countryID,
            'regionID' => $regionID,
            'countyID' => $countyID,
            'postcode' => $postcode));
        $breedsocID = $breedsoc->setBreedingSociety($row, $userID, 1, $addressID);

  //  print_r($row);
        echo "<br><pre>";
     //  print_r($results[0]->results[0]);
       echo "</pre><br>";


}
    $count ++;
    ob_flush();
    flush();
}
*/


// Script for loading sheep breed societies
/* $breedsoc = new uploadMarts();
$data = $breedsoc->loadFile("temp/data/sheep_societies.csv");
$count = 0;

foreach ($data as $row) {
    $address = $row[2];


    if ($count > -1) {
        $results = [];
        $address='';
        $results[] = $breedsoc->getGeocodedData($row);
        $userID = $breedsoc->setUser($row[5]);
        $addressParts = explode(',', $row[2]);
        $country = $row[3];
        if ($country=="Ireland") {
            $region = "republic of ireland";
        } else {
            $region = "United Kingdom";
        }
        $postcode = end($addressParts);
        $county = prev($addressParts);
        $city = prev($addressParts);
        $countAddressPartsRemaining = count($addressParts)-3;
        for ($i=0; $i<$countAddressPartsRemaining; $i++) {
            $address .= $addressParts[$i].", ";
        }

        $countryID = $breedsoc->setCountry($country);
        $regionID = $breedsoc->setRegion($region, $results, $countryID);
        $countyID = $breedsoc->setCounty($county, $results, $regionID, $countryID);
        $cityID = $breedsoc->setCity($city, $results, $countyID);
        $addressID = $breedsoc->setAddress($address, $results, array(
            'cityID' => $cityID,
            'countryID' => $countryID,
            'regionID' => $regionID,
            'countyID' => $countyID,
            'postcode' => $postcode));
        $breedsocID = $breedsoc->setBreedingSociety($row, $userID, 1, $addressID);


        ob_flush();
        flush();
    }

}
*/

// Script for loading Irish auctioneers
/* $irish = new uploadMarts();
$data = $irish->loadFile("temp/data/irish_marts.csv");
foreach ($data as $row) {
    $address = $row[1] . ", " . $row[2];
    $results = $irish->getGeocodedData($address);
    $fullAddress[] = $results->results[0]->formatted_address;
    $userID = $irish->setUser($row[5]);
    $addressParts = explode(',', $row[1]);
    $countryID = $irish->setCountry($row[2]);
    $regionID = $irish->setRegion($row[2], $results, $countryID);
    $county = end($addressParts);
    array_pop($addressParts);
    $countyID = $irish->setCounty($county, $results, $regionID, $countryID);
    $city = end($addressParts);
    array_pop($addressParts);
    $cityID = $irish->setCity($city, $results, $countyID);
    $address = end($addressParts);
    $addressID = $irish->setAddress($address, $results, array(
        'cityID' => $cityID,
        'countryID' => $countryID,
        'regionID' => $regionID,
        'countyID' => $countyID,
        'postcode' => ''));
    $companyID = $irish->setCompany($row[0], $row[5], $userID, $addressID, $row);
    $martID = $irish->setMart($row, $companyID, $addressID);



}
*/

