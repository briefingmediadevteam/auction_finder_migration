<?php

/**
 * Created by PhpStorm.
 * User: sthackeray
 * Date: 05/01/2016
 * Time: 10:19
 */
class listings
{

    public $mysqli;
    public $old_db_mysqli;
    public $address;
    public $details;
    public $file;
    public $dateStamp;

    function __construct()
    {
        $this->file = "temp/data/marts.csv";
        $this->mysqli = new mysqli('localhost', 'stephen', 'password', 'auction_finder');
        $this->old_db_mysqli = new mysqli('localhost', 'stephen', 'password', 'OLD_auction_finder');
        $this->dateStamp = date("Y-m-d h:m:s", time());
    }


    function checkIfExists($table, $col, $val)
    {
        $mysqli = $this->mysqli;
        $sql = "SELECT id," . $col . " FROM " . $table . " WHERE " . $col . " LIKE '%" . $val . "%'";
     //   echo $sql."<br>";
        if ($result = $mysqli->query($sql)) {
            //   echo $result->num_rows;
            if ($result->num_rows == 0) {
                $data = 0;
            } else {
                while ($row = $result->fetch_assoc()) {
                    $data = $row['id'];
                }
            }
            $result->close();
        } else die($mysqli->error);
        return $data;

    }


    function getOldListings()
    {
        $mysqli = $this->old_db_mysqli;
        $data = array();
        $details = array(
            "company" => '',
            "mart_name" => '',
            "cat_id" => '',
            "cat_id2" => '',
            "cat_id3" => '',
            "title" => '',
            "pre_description" => '',
            "description" => '',
            "auctionDate" => '',
            "time" => '',
            "mth" => '',
            "archive" => '',
        );

        $sql = "SELECT
                  (SELECT company FROM users WHERE id = listings.`user_id`) as company,
                  (SELECT mart_name FROM locations WHERE id = listings.loc_id) as mart_name,
                `cat_id`,
                `cat_id2`,
                `cat_id3`,
                `title`,
                `pre_description`,
                `description`,
                `auctionDate`,
                `time`,
                `featured`,
                `mth`,
                `archive`
                FROM listings
                ORDER BY loc_id ASC
        ";

        if ($result = $mysqli->query($sql)) {
            while ($row = $result->fetch_assoc()) {
                foreach ($row as $key=>$value) {
                    $details[$key] = $value;
                }
            $data[] = $details;
            }

            $result->close();
        }  else {
            die($mysqli->error);
        }
        return $data;
    }




    public function setListings($listingData) {
        $mysqli = $this->mysqli;
        $martID = 0;
        $breedingSocID = 0;
        $addressID = 0;
        $marts = [];
        $company = addslashes(str_replace(' and ', ' & ', $listingData['mart_name']));

        $martID = $this->checkIfExists('venues', 'venue_name', strtolower($company));
        if ($martID == 0) {
            $breedingSocID = $this->checkIfExists('breed_societies', 'society', strtolower($company));
            $addressID = $this->checkIfExists('address_to_breed_society', 'breed_society_id', $breedingSocID);
        } else {
            $addressID = $this->checkIfExists('address_to_venue', 'venue_id', $martID);
        }

        // Correct time formats
        $listTime = $listingData['time'];
        $time = str_replace(".", ":", $listTime);





        if ($addressID != 106) {
            $sql = "INSERT INTO listings VALUES(
              '',
              $martID,
              $breedingSocID,
              $addressID,
              '".$listingData['auctionDate']."',
              '".$time."',
              '".addslashes($listingData['title'])."',
              '".addslashes($listingData['pre_description'])."',
              '".addslashes($listingData['description'])."',
              '".$listingData['featured']."',
              '".$listingData['mth']."',
              '',
              '',
              '".$this->dateStamp."',
              '',
              '".$listingData['archive']."'
            )";
			echo $sql."<br>";
           
		   if ($result = $mysqli->query($sql)) {

            }  else {
                die($mysqli->error);
            }
        }
        $added = $this->mysqli->insert_id;
        return $added;
    }

    public function setCategories($catID, $listingID) {
        $mysqli = $this->mysqli;
        if ($catID[0] != 0) {
            $sql = "INSERT INTO category_to_listing VALUES(
              '',
              $catID[0],
              $listingID
          )";
            if ($result = $mysqli->query($sql)) {

            }  else {
                die($mysqli->error);
            }
        }

        if ($catID[1] != 0) {
            $sql = "INSERT INTO category_to_listing VALUES(
              '',
              $catID[1],
              $listingID
          )";
            if ($result = $mysqli->query($sql)) {

            }  else {
                die($mysqli->error);
            }

        }

        if ($catID[2] != 0) {
            $sql = "INSERT INTO category_to_listing VALUES(
              '',
              $catID[0],
              $listingID
          )";
            if ($result = $mysqli->query($sql)) {

            }  else {
                die($mysqli->error);
            }
        }

    }


}

$listings = new listings();
$old_listings = $listings->getOldListings();
foreach ($old_listings as $listingRow) {
  //  print_r($listingRow);
    $listing_id = $listings->setListings($listingRow);

    $categories = array();
    $categories[0] = $listingRow['cat_id'];
    $categories[1] = $listingRow['cat_id2'];
    $categories[2] = $listingRow['cat_id3'];
    $categoriesIDs = $listings->setCategories($categories, $listing_id);
    echo $listing_id."<br>";

}

